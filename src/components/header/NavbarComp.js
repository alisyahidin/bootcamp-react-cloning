import React from 'react';
import {
  Button,
  Collapse,
  Navbar,
  NavbarToggler,
  NavbarBrand,
  Nav,
  NavItem,
  NavLink } from 'reactstrap';

export default class NavbarComp extends React.Component {
    state = {
      isOpen: false,
      logo: 'http://i63.tinypic.com/oid9xu.png'
    }

    toggle = () => {
        this.setState({
            isOpen: !this.state.isOpen
        });
    }

    render() {
        return (
          <div>
            <Navbar className="py-2 text-center" color="light" light expand="md" fixed="top">
              <NavbarBrand href="/">
                <img className="mr-2" src={this.state.logo} width="40" alt="Logo"/>
                Santren Koding
              </NavbarBrand>
              <NavbarToggler onClick={this.toggle} />
              <Collapse isOpen={this.state.isOpen} navbar>
                <Nav className="ml-auto" navbar>
                  <NavItem>
                    <NavLink href="#">Home</NavLink>
                  </NavItem>
                  <NavItem>
                    <NavLink href="#">Kajian Koding</NavLink>
                  </NavItem>
                  <NavItem>
                    <NavLink href="#">Santren Kilat</NavLink>
                  </NavItem>
                  <NavItem>
                    <NavLink href="#">Mondok</NavLink>
                  </NavItem>
                  <NavItem>
                    <NavLink href="#">Tentang Kami</NavLink>
                  </NavItem>
                  <NavItem>
                    <Button outline color="primary">Login / Signup</Button>
                  </NavItem>
                </Nav>
              </Collapse>
            </Navbar>
          </div>
        );
    }
}