import React, { Component } from 'react';
import NavbarComp from './NavbarComp';
import JumbotronComp from './JumbotronComp';

export default class Header extends Component {
    render() {
        return(
            <div>
                <NavbarComp />
                <JumbotronComp />
            </div>
        )
    }
}