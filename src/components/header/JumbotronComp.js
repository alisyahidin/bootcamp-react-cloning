import React from 'react';
import { Jumbotron, Button } from 'reactstrap';
import './JumbotronComp.css';

const JumbotronComp = (props) => {
  return (
    <div>
      <Jumbotron className="text-center text-white jumbotron-display mt-5">
        <h1 className="pt-5">Belajar Jadi Asyik!</h1>
        <p className="py-2 lead">Kegiatan Belajar Intensif Programming dan Qur'an bersama Santren Koding.</p>
        <p className="py-4 lead">
          <Button size="lg" color="success">Mulai belajar</Button>
        </p>
      </Jumbotron>
    </div>
  );
};

export default JumbotronComp;