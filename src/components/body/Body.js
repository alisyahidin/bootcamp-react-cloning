import React, { Component } from 'react';
import { Button, Container, Row, Col } from 'reactstrap';
import './Body.css';

import CardComp from './CardComp';

export default class Body extends Component {
  state = {
    cards: [
      {
        title: 'Kajian Koding #3',
        description: 'ReactJS dan Firebase Auth/Hosting',
        image: 'https://lh3.googleusercontent.com/XLlyoc7OLR2dgFdnhKk-83-6_kxoTXu-6RyPcUiFc_UiSwZmuIvibNfS307Crb3IcFSbPjq1EdHNIknl-_4N5QAVpBFAZmGeC3d6ZMLwng5eAhnKBnoD1V-pcItiFukHZIq0UgA62NvO_Yg'
      },
      {
        title: 'Kajian Koding #2',
        description: 'html, css, boostrap dasar',
        image: 'https://lh5.googleusercontent.com/-JajpG87BjXAxqMCAu7bs_Bb-c84GUdFtG9w0mNB_dQ1oaOnT3aRYYllYr9tHnB-rLJ8ZxpOHA=w2381'
      },
      {
        title: 'Kajian Koding #1',
        description: 'Belajar Laravel Dasar',
        image: 'https://lh5.googleusercontent.com/Nw8x_CE9s4N8WGggkhxit5nHdo8j03kr0daRbRTRhduOtJzwmaw3h-iR0T4iBiXrEyQqN-q_VA=w3572'
      }
    ]
  }

  render() {
    return (
      <div>
        <Container>
          <h2 className="text-center py-3">Daftar Kajian Koding Rutin</h2>
          <Row>
            {this.state.cards.map((card, index) => {
              return (<CardComp
                key={index}
                cardTitle={card.title}
                cardDescription={card.description}
                cardImage={card.image}
                cardCol={{md: 4, sm: 12}}
              />)
            })}
          </Row>
          <Row className="my-4">
            <Col className="text-center">
              <Button outline color="success">Lihat Seluruh Kajian Koding</Button>
            </Col>
          </Row>
          <Row className="row-quote my-4">
            <Col style={{backgroundColor: '#1CD139'}} className="text-white text-center py-4" md="6" sm="12">
              <h3 className="mb-3">Kegiatan</h3>
              <h5>Koding</h5>
              <h5>Belajar Agama dan Al Quran</h5>
              <h5>Pendidikan Karakter</h5>
            </Col>
            <Col style={{backgroundColor: '#F3F3F3'}} className="text-center py-2" md="6" sm="12">
              <img src="http://i63.tinypic.com/oid9xu.png" alt="Logo"/>
              <h3>"Memberi Manfaat Untuk Ummat"</h3>
              <p>-Santren Koding-</p>
            </Col>
          </Row>
          <Row className="my-4 pt-3">
            <Col className="text-center">
              <h3>Mondok by Santren Koding</h3>
              <p>Sebuah Kegiatan Belajar Intensif Programming & Al Qur'an Selama 3 Tahun hingga jaminan kerja.</p>
              <CardComp
                cardImage={'http://i67.tinypic.com/2m2gkyp.png'}
                cardCol={
                  { md: {
                    size: 4,
                    offset: 4
                  },
                    sm: 12
                  }
                }
              />
              <Button outline color="success" className="mt-4">Lihat Selengkapnya</Button>
            </Col>
          </Row>
        </Container>
      </div>
    );
  }
}
