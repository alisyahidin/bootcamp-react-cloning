import React, { Component } from 'react';
import { Card, CardImg, CardBody,
  CardTitle, CardSubtitle, Col } from 'reactstrap';
import './CardComp.css';

export default class CardComp extends Component {
  render() {
    return (
      <Col md={this.props.cardCol.md} sm={this.props.cardCol.sm}>
        <Card className="card-info">
          <CardImg top width="100%" src={this.props.cardImage} alt="Card image" />
          <CardBody>
            <CardTitle>{this.props.cardTitle}</CardTitle>
            <CardSubtitle>{this.props.cardDescription}</CardSubtitle>
          </CardBody>
        </Card>
      </Col>
    );
  }
};