import React from 'react';
import { Container, Col } from 'reactstrap';

const ContactComp = (props) => {
  return(
    <div>
      <Container fluid>
        <Container className="text-center py-5">
          <Col md={{ size: 6, offset: 3 }} sm="12">
            <h3>Kontak</h3>
            <p>{props.contact.phone}</p>
            <p>{props.contact.email}</p>
            <p>{props.contact.address}</p>
          </Col>
        </Container>
      </Container>
    </div>
  )
}

export default ContactComp;