import React from 'react';
import { Container, Row, Col } from 'reactstrap';

const SponsorComp = (props) => {
  return (
    <div>
      <Container>
        <Row>
          <Col className="text-center py-3">
            <h3>Sponsor & Partner</h3>
          </Col>
        </Row>
        <Row className="my-3">
          <Col md={ props.sponsors.row1.size.md } sm={ props.sponsors.row1.size.sm }>
            <img className="img-fluid" src={ props.sponsors.row1.image } alt="KliniKita"/>
          </Col>
        </Row>
        <Row className="my-5 text-center">
          { props.sponsors.row2.image.map(image => {
            return (
              <Col md={ props.sponsors.row2.size.md } sm={ props.sponsors.row2.size.sm }>
                <img className="py-3" src={ image } alt="KliniKita" height="120"/>
              </Col>
            )
          }) }
        </Row>
      </Container>
    </div>
  );
}

export default SponsorComp;
