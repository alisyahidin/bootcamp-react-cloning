import React, { Component } from 'react';
import { Container } from 'reactstrap';

import ContactComp from './ContactComp';
import SponsorComp from './SponsorComp';

export default class Footer extends Component {
  state = {
    contact: {
      phone: '082225111587',
      email: 'santrenkoding@gmail.com',
      address: 'Titik Ruang Space, Jl. Ngesrep Tim. III No.67, Sumurboto, Banyumanik, Kota Semarang, Jawa Tengah 50269'
    },
    sponsors: {
      row1: {
        image: 'http://i67.tinypic.com/2hcn70k.jpg',
        size: {
          md: {
            size: 4,
            offset: 4
          },
          sm: 12
        }
      },
      row2: {
        image: [
          'https://1.bp.blogspot.com/-GZjl-D1QU6k/WgpvD8krquI/AAAAAAAAE0Y/tkrHzHHdt-4BdvmofUT7vuNtjG4ANIRPQCLcBGAs/s400/Undip.png',
          'https://adiwibowo.files.wordpress.com/2012/10/logo-udinus.png?resize=370%2C358',
          'https://upload.wikimedia.org/wikipedia/id/archive/6/6a/20150926142003%21Logo_unisbank.jpg',
          'http://id.indonesiayp.com/img/id/c/1445918663-86-pt-java-valley-technology.png',
          'https://dynamiclearningid.files.wordpress.com/2017/01/orderdilla.png?w=500',
          'https://www.go-mekanik.com/assets/public/src/imgs/gomekanik/logo.png'
        ],
        size: {
          md: 2,
          sm: 4
        }
      }
    }
  }

  render() {
    return(
      <div>
        <SponsorComp sponsors={this.state.sponsors}/>
        <Container fluid className="bg-light">
          <ContactComp contact={this.state.contact}/>
          <Container>
            <p>Copyright © Santren Koding 2018. All rights reserved.</p>
          </Container>
        </Container>
      </div>
    )
  }
}